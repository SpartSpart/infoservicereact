interface DiagnosticDto{
    id:number,
    name: string,
    description:string,
    recommendation:string,
    type:string,
    minioFilesLink:string,
    date:string

}