'use client'
import Link from "next/link";
import {useRouter} from "next/navigation";
import {createContext} from 'react';
import TextAreaTestComponent from "./garbage/TextAreaTestComponent";

// export const contextTest = createContext('initialContext');

export default function Home() {
    const router = useRouter()
    return (
        <main>
            {/*<contextTest.Provider value="CONTEXT">*/}
                <h1>Hello world_2</h1>
                <Link href="/diagnostic" className='link link-hover link-primary'> User</Link>
                <br/>
                <button className="btn btn-primary" onClick={() => {
                }}>BUTTON
                </button>
                <br/>
                <button className="btn btn-secondary" type="button" onClick={() => router.push('diagnostic')}>
                    BUTTON_2
                </button>
                <br/>
                <TextAreaTestComponent/>
            {/*</contextTest.Provider>*/}


        </main>
    );

}

