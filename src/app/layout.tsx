import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import Modal from "./diagnostic/modal/Modal";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Create Next App",
  description: "Generated by create next app",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" data-theme = 'cupcake'>
      <body className={inter.className}>{children}</body>
      <Modal/>
    </html>
  );
}

// export default function RootLayout({
//                                      children,
//                                    }: {
//   children: React.ReactNode
// }) {
//   return (
//       <html lang="en">
//       <body className={inter.className}>
//       {children}
//       <Modal/>
//       </body>
//       </html>
//   )
// }
