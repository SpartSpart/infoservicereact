import React from 'react'

interface Props{
    s:string
}

const DiagnosticEdit = (props: Props) => {
    return (
        <header>
            <h1>
                {props.s}
            </h1>
        </header>
    )
}

export default DiagnosticEdit