import React, {createContext} from 'react'

export const contextTest2 = createContext('');

const TextAreaTestComponent = () => {

  return (
    <textarea>VALUE</textarea>
  )
}

export default TextAreaTestComponent