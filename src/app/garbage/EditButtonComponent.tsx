'use client'
import React, {useContext, useState} from 'react'

interface Props {
    diagnostic: DiagnosticDto

}

const EditButtonComponent = (props: Props) => {
    // const detailedInfoIsVisible = useContext(diagnosticContext);
    const [expanded, setExpanded] = useState(false);


    const handleClick = () => {
        setExpanded(!expanded)
    }

    return (
        <td>
            <button className='btn btn-primary' onClick={handleClick}>EDIT</button>
            {
                expanded && getMoreInfo(props.diagnostic)
            }
        </td>

    )
}

function getMoreInfo(diagnostic: DiagnosticDto) {
   
    console.log('Button pressed')
    console.log({ diagnostic })
    console.log(true)
    return (
        <></>

        // <DiagnosticSolidComponentDetailed diagnosticDto.tsx={diagnostic} key={diagnostic.id}/>
    )
}

export default EditButtonComponent