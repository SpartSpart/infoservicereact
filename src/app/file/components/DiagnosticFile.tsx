import React, {useCallback, useEffect, useState} from 'react';
import Button from '@mui/material/Button';
import Image from 'next/image'
import fileIcon from "./file-logo.png";
import {width} from "@mui/system";
import {Icon, IconButton} from "@mui/material";
import styles from "../css/file.module.css";

interface Props {
    fileName: string
    bucket: string|undefined

}

let base64 = require("base-64");

const DiagnosticFile = (props: Props) => {

    const [clicked, setClicked] = useState(false)

    const [file, setFile] = useState<Blob>()

    const handleClick = () => {
        setClicked(!clicked)
    }

    useEffect(() => {
        async function getFile() {
            if (clicked) {
                console.log('startAsyncDOWNLOAD')
                const login = 'John'
                const password = 'password'

                const credential = "Basic " + Buffer.from(login + ":" + password).toString("base64");

                const res = await fetch(`http://localhost:8081/api/minio/${props.bucket}/${props.fileName}`,
                    {
                        method: 'GET',
                        cache: 'no-store',
                        headers: {
                            Authorization: "Basic " + base64.encode(login + ":" + password),
                        },
                    })
                    ////////////
                    .then(res => res.blob())
                    .then(blob => {
                        var file = window.URL.createObjectURL(blob);
                        window.location.assign(file);
                    });
                ///////////

                console.log('end')
                setClicked(false)
            }

        }

        getFile()
    }, [clicked])



    return (

        <div>
            <Image className={styles.image}
                   onClick={handleClick}
                   src={fileIcon}
                   alt={props.fileName}
                   width="48" height="48"/>
            <label className={styles.label}>{props.fileName}</label>

            {/*<button onClick={handleClick}> GET FILE</button>*/}
            {/*<button onClick={handleClick}> Contained</button>*/}
        </div>
    )
        ;
};

export default DiagnosticFile;

