import React, {useEffect, useState} from 'react'
import DiagnosticFile from "@/app/file/components/DiagnosticFile";
import {Simulate} from "react-dom/test-utils";
import error = Simulate.error;

interface Props {
    bucket: string|undefined
}

export default function FileListCustom(props: Props) {

     // const auth = 'Basic ' + base64.encode(username + ":" + password);

    const [files, setFiles] = useState<[]>([])

    console.log('START')
    let base64 = require("base-64");

    useEffect(() => {
        async function getFileList() {
            console.log('startAsync')
            const login = 'John'
            const password = 'password'

            // const credential = "Basic " + Buffer.from(login + ":" + password).toString("base64");

            const res = await fetch(`http://localhost:8081/api/minio/${props.bucket}`,
                {
                    method: 'GET',
                    // mode: 'no-cors',
                    cache: 'no-store',
                    // body: JSON.stringify({ login, password })
                    // headers: {
                    //     Authorization: credential,
                    // },
                    headers: {
                        Authorization: "Basic " + base64.encode(login + ":" + password),
                    },
                })
            console.log('PreEnd')

             setFiles(await res.json())

            // await res.json().then((files) => setFiles(files => [...files])).catch(e => console.log(e))
            console.log('end')


        }
        console.log(props.bucket)
        props.bucket?.length && getFileList().catch(error)
    },[props.bucket])

    return (

        <div>
            {/*{JSON.stringify(files,null,2)}*/}
            {(files.length) && files.map(file => <DiagnosticFile key = {Math.random()} fileName={file} bucket={props.bucket}/>)}

        </div>
    )

}