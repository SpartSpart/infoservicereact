interface DiagnosticDto{
    id:number,
    type:string,
    name:string,
    description:string,
    recommendation:string,
    date:string,
    bucket:string
}