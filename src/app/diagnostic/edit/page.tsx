'use client'
import React, {useCallback, useEffect, useState} from 'react';
import Link from "next/link";
import {usePathname, useRouter, useSearchParams} from "next/navigation";
import FileListCustom from "@/app/file/components/FileListCustom";
import {Simulate} from "react-dom/test-utils";
import {TextareaAutosize} from "@mui/material";
import styles from "./css/editdiagnostic.module.css"


const EditDiagnostic = (props: DiagnosticDto) => {
    console.log(props)

    const router = useRouter();

    const rec = localStorage.getItem('diagnostic')

    const pathname = usePathname();

    const [diagnostic, setDiagnostic] = useState<DiagnosticDto | null>(null)

    const searchParams = useSearchParams()

    const diagnosticId = searchParams.get('diagnosticId')

    let base64 = require("base-64");

    useEffect(() => {
        (async function getDiagnosticById() {
            console.log('startAsync')
            const login = 'John'
            const password = 'password'

            if (diagnosticId) {
                const res = await fetch(`http://localhost:8081/api/diagnostic/${diagnosticId}`,
                    {
                        method: 'GET',
                        // mode: 'no-cors',
                        cache: 'no-store',

                        headers: {
                            Authorization: "Basic " + base64.encode(login + ":" + password)


                        },
                    })
                console.log('PreEnd')

                setDiagnostic(await res.json())

                // await res.json().then((files) => setFiles(files => [...files])).catch(e => console.log(e))
                console.log('end')
            }


        })()

    }, [])


    function handleClick() {
        editDiagnostic();
    }


    const changeFieldHandler = useCallback((e) => {
        // const {name, value} = e.target;


        setDiagnostic({...diagnostic, [e.target.name]: e.target.value});

        // console.log(`TYPE AFTER: ${[e.target.name]} : ${diagnostic}`);
    }, [diagnostic]);

    return (


        <div className={styles.input}>


            <h1>NEW OR EDIT PAGE</h1>
            <div>
                <label className={styles.label}>id</label>
                <TextareaAutosize onChange={changeFieldHandler} className={styles.input} name='id'
                                  defaultValue={diagnostic?.id}
                                  placeholder='id'/>
            </div>
            <div>
                <label className={styles.label}>type</label>
                <TextareaAutosize onChange={changeFieldHandler} className={styles.input} name='type'
                                  defaultValue={diagnostic?.type} placeholder='type'/></div>
            <div className="styles.input-box">
                <label className={styles.label}>name</label>
                <TextareaAutosize onChange={changeFieldHandler} className={styles.input}
                                  name='name'
                                  defaultValue={diagnostic?.name} placeholder='name'/>
            </div>
            <div className="styles.input-box">
                <label className={styles.label}>description</label>
                <TextareaAutosize onChange={changeFieldHandler} className={styles.input}
                                  name='description'
                                  defaultValue={diagnostic?.description}
                                  placeholder='description'/>
            </div>
            <div className="styles.input-box">
                <label className={styles.label}>recommendation</label>
                <TextareaAutosize onChange={changeFieldHandler} className={styles.input}
                                  name='recommendation'
                                  defaultValue={diagnostic?.recommendation}
                                  placeholder='recommendation'/>
            </div>
            <div className="styles.input-box">
                <label className={styles.label}>bucket</label>
                <TextareaAutosize onChange={changeFieldHandler} className={styles.input}
                                  name='bucket'
                                  defaultValue={diagnostic?.bucket}
                                  placeholder='bucket'/>
            </div>
            <div className="styles.input-box">
                <label className={styles.label}>date</label>
                <TextareaAutosize onChange={changeFieldHandler} className={styles.input}
                                  name='date'
                                  defaultValue={diagnostic?.date}
                                  placeholder='date'/>
            </div>

            < FileListCustom bucket={diagnostic?.bucket}/>

                <button type="button" className="bg-red-500" onClick={handleClick}>Save</button>
       </div>
    );

    async function editDiagnostic() {
        const query = {
            "id": diagnostic?.id,
            "name": diagnostic?.name,
            "description": diagnostic?.description,
            "recommendation": diagnostic?.recommendation,
            "type": diagnostic?.type,
            "date": diagnostic?.date
        }

        console.log('startAsync ' + 'diagnosticId: ' + 22222)
        const login = 'John'
        const password = 'password'

        //TODO
        if (true) {
            const res = await fetch(`http://localhost:8081/api/diagnostic`,
                {
                    method: 'PUT',
                    // mode: 'no-cors',
                    cache: 'no-store',
                    body: JSON.stringify(query),
                    headers: {
                        Authorization: "Basic " + base64.encode(login + ":" + password),
                        "Content-Type": 'application/json'
                    },
                })

            if (res.ok) {
                console.log('Diagnostic updated successfully');
                // router.push("/diagnostic");
                router.back()
            } else
                console.log('Fail to update diagnostic')

        }
    }
};


export default EditDiagnostic;
