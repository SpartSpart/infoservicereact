
import React, {useContext} from 'react'
import DiagnosticListComponent from './components/DiagnosticListComponent';
import {DiagnosticContext} from "@/app/diagnostic/components/DiagnosticSolidComponent";

const DiagnosticPage = async () => {

    return (
        <div>
            <DiagnosticListComponent/>
        </div>
    )
}

export default DiagnosticPage