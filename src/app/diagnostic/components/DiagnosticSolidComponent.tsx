'use client'
import React, {createContext, useState} from 'react'
import DiagnosticSolidComponentDetailed from './DiagnosticSolidComponentDetailed'
import NewDiagnosticBtn from "@/app/diagnostic/components/NewDiagnosticBtn";
import styles from "./css/diagnostic.module.css"

interface Props {
    diagnostic: DiagnosticDto
}

type Context = {
    diagnostic: DiagnosticDto;
};

export const DiagnosticContext = createContext<Context | null>(null)
// export const DiagnosticContext = createContext<Context | null>(null);

const DiagnosticSolidComponent = (props: Props) => {

    const [showDetailedInfo, setShowDetailedInfo] = useState(false);

    const contextValue = {
        diagnostic: props.diagnostic
    };

    const handleClick = () => {
        setShowDetailedInfo(!showDetailedInfo)
    }


    return (

            <div className={"border-2"}>
                {/*<DiagnosticContext.Provider value={contextValue}>*/}
                    <div>
                        {getNotEditableFields(props.diagnostic)}
                        < button className='btn btn-primary' onClick={handleClick}>EDIT</button>
                    </div>

                    {showDetailedInfo && <DiagnosticSolidComponentDetailed diagnostic={props.diagnostic}/>}
                    <br/>
                    <NewDiagnosticBtn diagnostic={props.diagnostic} btnName={"Edit Diagnostic"}/>
                {/*</DiagnosticContext.Provider>*/}
            </div>

    )
}


function getNotEditableFields(diagnostic: DiagnosticDto) {
    return (
        <div>

            <div className="input-box">
                <label>id</label>
                <input type='text' name='id' defaultValue={diagnostic.id} readOnly={true}/>
            </div>
            <div className="input-box">
                <label>type</label>
                <input type='text' name='type' defaultValue={diagnostic.type} readOnly={true}/>
            </div>
            <div className="input-box">
                <label>date</label>
                <input type='text' name='date' defaultValue={diagnostic.date} readOnly={true}/>
            </div>
            <div className="input-box">
                <label>name</label>
                <input type='text' name='name' defaultValue={diagnostic.name} readOnly={true}/>
            </div>


        </div>
    )

}


export default DiagnosticSolidComponent