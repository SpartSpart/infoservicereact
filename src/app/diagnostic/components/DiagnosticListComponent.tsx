import React from 'react'
import DiagnosticSolidComponent from './DiagnosticSolidComponent';
import styles from './css/diagnostic.module.css'
import NewDiagnosticBtn from "@/app/diagnostic/components/NewDiagnosticBtn";


const DiagnosticListComponent = async () => {

    let username: 'John';
    let password: 'Malkovich';
    const res = await fetch('http://localhost:8081/api/diagnostic/test',
        {
            method: 'GET',
            mode: 'no-cors',
            cache: 'no-store'
            // headers: { 'Content-Type': 'application/json' },

        })

    const diagnosticList = await res.json()
    console.log({diagnosticList})


    return (

        <div>
            <h1>Diagnostics</h1>
            <br/>
            {diagnosticList.length > 0
                ? getAllDiagnostics(diagnosticList)
                : <h1>There are no topics yet</h1>
            }
        </div>

    )
}


function getAllDiagnostics(diagnosticList: DiagnosticDto[]) {
    return (
        <div>
            {
                diagnosticList.map(diagnostic => <DiagnosticSolidComponent diagnostic={diagnostic}
                                                                           key={diagnostic.id}/>)
            }
            <div style={{float:"right"}} id="elt">
                <NewDiagnosticBtn diagnostic={null} btnName={"New Diagnostic"}/>
            </div>
        </div>
    )
}

export default DiagnosticListComponent
