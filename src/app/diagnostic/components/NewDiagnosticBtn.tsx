'use client'
import Link from 'next/link';
import React, {createContext} from 'react';
import {router} from "next/client";
import {useRouter} from "next/navigation";



interface Props {
    diagnostic: DiagnosticDto|null
    btnName:string
}

export default function NewDiagnosticBtn(props: Props) {
    const router = useRouter()
    function handleClick() {
        router.push('/diagnostic/edit')
    }
    return (
        <div>

                <Link href={{
                        pathname: '/diagnostic/edit',
                        query: {diagnosticId: props?.diagnostic?.id}}}>
                    <button className='btn btn-primary'>
                        {props.btnName}
                    </button>
                </Link>

                {/*<button onClick={handleClick} className='btn btn-primary'>EDIT DIAGNOSTIC</button>*/}

        </div>
    );
};

// export default NewDiagnosticBtn;
