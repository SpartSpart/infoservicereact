import React, { useEffect, useState } from 'react'
import styles from './DiagnosticComponent.module.css'
import DiagnosticSolidComponent from './DiagnosticSolidComponent';


const DiagnosticListComponent = async () => {

    // const[diagnosticList, setDiagnosticList] = useState({})

    // useEffect(()=>{

    //     (async function getDiagnosticList(){

    //     let username: 'John';
    // let password: 'Malkovich';
    // const res = await fetch('http://localhost:8081/api/diagnostic/test',
    //     {
    //         method: 'GET',
    //         mode: 'no-cors',
    //         cache: 'no-store'
    //     })
    // setDiagnosticList(await res.json());
    // console.log({diagnosticList})
    //     })()

    // })


    let username: 'John';
    let password: 'Malkovich';
    const res = await fetch('http://localhost:8081/api/diagnostic/test',
        {
            method: 'GET',
            mode: 'no-cors',
            cache: 'no-store'
        })

    const diagnosticList = await res.json()
    console.log({ diagnosticList })


    return (

        <div>
            <h1>Diagnostics</h1>
            {diagnosticList.length > 0
                ? getAllDiagnostics(diagnosticList)
                : <h1>There are no topics yet</h1>
            }
        </div>
    )
}

function getAllDiagnostics(diagnosticList: DiagnosticDto[]) {
    return (


        // <table>
        //     <thead>
        //         <tr>
        //             <th>Id</th>
        //             <th>Name</th>

        //             <th>Type</th>
        //         </tr>


        // </thead>
        <>

            {
                diagnosticList.map(diagnostic => <DiagnosticSolidComponent diagnostic={diagnostic} key={diagnostic.id} />)
            }
        </>
        // </table>



    )
}

export default DiagnosticListComponent
