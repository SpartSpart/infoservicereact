import TextField from '@mui/material/TextField';
import React from 'react';

interface Props{
    label:string
    defaultValue:string
    helperText:string

}

const Input = (props:Props) => {
    return (
        <TextField
            id="filled-helperText"
            label={props.label}
            defaultValue={props.defaultValue}
            helperText={props.helperText}
            variant="filled"
        />
    );
};

export default Input;
