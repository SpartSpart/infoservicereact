'use client'
import React, { useState } from 'react'
import DiagnosticSolidComponentDetailed from './DiagnosticSolidComponentDetailed'

interface Props {
    diagnostic: DiagnosticDto
}


const DiagnosticSolidComponent = (props: Props) => {

    const [showDetailedInfo, setShowDetailedInfo] = useState(false);


    const handleClick = () => {
        setShowDetailedInfo(!showDetailedInfo)
    }


    return (

        <>
            <div>
                {showDetailedInfo ? getEditableFields(props.diagnostic) : getNotEditableFields(props.diagnostic)}

                <div>< button className='btn btn-primary' onClick={handleClick}>EDIT</button></div>
            </div>

            {showDetailedInfo && <DiagnosticSolidComponentDetailed diagnostic={props.diagnostic} />}
            <br/>
        </>


    )
}



function getEditableFields(diagnostic: DiagnosticDto) {
    return (
        // <>
        //         <textarea>{diagnostic.id}</textarea>

        //     <textarea>{diagnostic.name}</textarea>
        //     <textarea>{diagnostic.type}</textarea>
        //     <textarea>{diagnostic.date}</textarea>
        // </>

        <div>
            <div>{diagnostic.id}</div>
            <div>{diagnostic.name}</div>
            <div>{diagnostic.type}</div>
            <div>{diagnostic.date}</div>
        </div>
    )

}

function getNotEditableFields(diagnostic: DiagnosticDto) {
    return (
        <div>
            <div>{diagnostic.id}</div>
            <div>{diagnostic.name}</div>
            <div>{diagnostic.type}</div>
            <div>{diagnostic.date}</div>
        </div>
    )

}



export default DiagnosticSolidComponent