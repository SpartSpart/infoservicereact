// 'use client'
import React, { useContext, useEffect, useState } from 'react'
import FileListCustom from '../files/FileListCustom'


interface Props {
    diagnostic: DiagnosticDto
}

const DiagnosticSolidComponentDetailed = (props: Props) => {
    //  const [isVisible, setIsVisible] = useState(props.isVisible);
    //  const isVisible = useContext(diagnosticContext);


    return (
        <>
            <div>
                <div>{props.diagnostic.description}</div>
                <div>{props.diagnostic.recommendation}</div>
                <div>{props.diagnostic.type}</div>
                <div>{props.diagnostic.minioFilesLink}</div>
            </div>
            <FileListCustom bucket={props.diagnostic.minioFilesLink} />
         
        </>

    )
}

export default DiagnosticSolidComponentDetailed