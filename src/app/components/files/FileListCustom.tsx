'use client'
import React, { useEffect, useState } from 'react'
import { ListFiles } from '../files/model/ListFiles'
import List from 'postcss/lib/list'

interface Props {
    bucket: string
}

const FileListCustom = (props: Props) => {

    const [files, setFiles] = useState({})

    console.log('START')

    useEffect(() => {
        (async function getFileList() {
            console.log('startAsync')
            const login = 'John'
            const password = 'password'

            // // const res = await fetch('http://localhost:8081/api/minio/${props.bucket}',
            // const res = await fetch('http://localhost:8081/api/diagnostic/test',
            //     {
            //         method: 'GET',
            //         mode: 'no-cors',
            //         //cache: 'no-store',
            //         // headers: { 'Content-Type': 'application/json' },
            //         // body: JSON.stringify({ login, password }),

            //     }
            // );

            const res = await fetch('http://localhost:8081/api/diagnostic/test',
                {
                    method: 'GET',
                    mode: 'no-cors',
                    cache: 'no-store'
                })
            const diagnosticList = await res.json();

            if (!res.ok) {
                throw new Error(res.status);
              }
              return res.json();

            console.log({res})
            console.log(await res.json())

            
            setFiles(diagnosticList);
            // console.log('Files: ' + files)
            // console.log('YYYYYYYYYY')
        })()
    }, [files])

    return (

        <div>
            {JSON.stringify(files)}
        </div>
    )

}

export default FileListCustom